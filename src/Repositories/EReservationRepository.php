<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Repositories;

use MathildeGrise\Recrutement\KataRefacto\Models\EReservation;

class EReservationRepository
{
    /**
     * @var EReservation[]
     */
    private array $eReservations;

    /**
     * @param EReservation[] $eReservations
     */
    public function __construct(array $eReservations)
    {
        $this->eReservations = $eReservations;
    }

    public function nextId(): int
    {
        return count($this->eReservations) + 1;
    }

    public function save(EReservation $eReservation): void
    {
        $this->eReservations[$eReservation->getId()] = $eReservation;
    }
}
