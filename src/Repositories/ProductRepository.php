<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Repositories;

use MathildeGrise\Recrutement\KataRefacto\Models\Product;

class ProductRepository
{
    /**
     * @var Product[]
     */
    private array $products;

    /**
     * @param Product[] $products
     */
    public function __construct(array $products)
    {
        $this->products = $products;
    }
}
