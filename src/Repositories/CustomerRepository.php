<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Repositories;

use MathildeGrise\Recrutement\KataRefacto\Models\Customer;

class CustomerRepository
{
    /**
     * @var Customer[]
     */
    private array $customers;

    /**
     * @param Customer[] $customers
     */
    public function __construct(array $customers)
    {
        $this->customers = $customers;
    }
}
