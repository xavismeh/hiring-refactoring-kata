<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto;

use Exception;
use MathildeGrise\Recrutement\KataRefacto\Models\Product;

class HttpStockService implements ProductAvailabilityInterface
{
    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function getStockLevelByStore(int $storeId, Product $product): array
    {
        // # YOU MUST NOT MAKE CHANGES HERE #

        // HTTP implementation

        throw new Exception('Stock Service HTTP API timeout');
    }
}
