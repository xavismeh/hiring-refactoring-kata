<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto;

use Exception;
use MathildeGrise\Recrutement\KataRefacto\EventSubscriber\EReservationSubscriber;
use MathildeGrise\Recrutement\KataRefacto\Framework\Response;
use MathildeGrise\Recrutement\KataRefacto\Models\Customer;
use MathildeGrise\Recrutement\KataRefacto\Models\EReservation;
use MathildeGrise\Recrutement\KataRefacto\Models\Product;
use MathildeGrise\Recrutement\KataRefacto\Models\Store;
use MathildeGrise\Recrutement\KataRefacto\Repositories\EReservationRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateReservation
{
    private LoggerInterface $logger;
    private ProductAvailabilityInterface $productAvailability;
    private PriceModifier $priceModifier;
    private EReservationRepository $EReservationRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        LoggerInterface $logger,
        ProductAvailabilityInterface $productAvailability,
        PriceModifier $priceModifier,
        EReservationRepository $EReservationRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->logger = $logger;
        $this->productAvailability = $productAvailability;
        $this->priceModifier = $priceModifier;
        $this->EReservationRepository = $EReservationRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function create(Product $product, Customer $customer, Store $currentStore): Response
    {
        $this->logger->info('init create E-reservation process', ['product' => $product, 'customer' => $customer]);
        $this->logger->debug('Determine if there is stock for the product on the store');

        $response = new Response();
        $currentStoreId = $currentStore->getId();

        try {
            $productAvailability = $this->productAvailability->getStockLevelByStore($currentStoreId, $product);
        } catch (Exception $e) {
            $this->logger->error("Error StockByStore", ['error' => $e->getMessage()]);

            return $response->setCode(500);
        }

        // Check availability
        if (!$productAvailability['Available']) {
            $this->logger->warning('Product unavailable', ['product' => $product, 'store' => $currentStore]);

            return $response->setCode(412);
        }

        // Persist new e-reservation in DB
        $this->logger->debug('Create new E-reservation');
        $eReservation = new EReservation(
            $this->EReservationRepository->nextId(),
            $currentStoreId,
            $product->getSKU(),
            $this->priceModifier->modify($product),
            $customer->getId()
        );
        $this->EReservationRepository->save($eReservation);

        $this->eventDispatcher->dispatch($eReservation, EReservationSubscriber::ERESERVATION_NEW);

        $this->logger->info('E-reservation created with success', ['product' => $product, 'customer' => $customer]);

        return $response
            ->setCode(201)
            ->setData(['id' => $eReservation->getId()]);
    }
}
