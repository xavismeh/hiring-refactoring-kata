<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto;

use Exception;

class HttpMailer
{
    /**
     * @throws Exception
     */
    public function sendNewEReservation(string $email, int $id)
    {
        // # YOU MUST NOT MAKE CHANGES HERE #

        // Send Data over HTTP to a Mail Provider API.
        throw new Exception('Your Token API is invalid. You can create your token from our web interface.');
    }
}
