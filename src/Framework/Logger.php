<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Framework;

use Psr\Log\AbstractLogger;

class Logger extends AbstractLogger
{
    /**
     * @throws \JsonException
     */
    public function log($level, $message, array $context = array()): void
    {
        echo($level . " : " . $message . "\n");
        echo(json_encode($context, JSON_THROW_ON_ERROR) . "\n");
    }
}
