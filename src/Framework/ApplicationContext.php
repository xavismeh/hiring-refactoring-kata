<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Framework;

use MathildeGrise\Recrutement\KataRefacto\Models\Store;

class ApplicationContext
{
    /**
     * @var self
     */
    protected static $instance;

    /**
     * @var Store
     */
    public static $currentStore;

    /**
     * @var array
     */
    public static array $config;

    private function __construct()
    {
    }

    public static function getInstance(): self
    {
        if (!self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    public function getCurrentStore(): Store
    {
        return self::$currentStore;
    }

    public function getConfig(): array
    {
        return self::$config;
    }
}
