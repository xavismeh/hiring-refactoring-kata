<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Framework;

class Application_ServiceLocator
{
    public static $services = [];

    /**
     * @param string $service The service key name
     *
     * @return mixed Any object
     */
    public static function get(string $service): mixed
    {
        return self::$services[$service];
    }
}
