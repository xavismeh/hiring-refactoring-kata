<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto;

use MathildeGrise\Recrutement\KataRefacto\Models\Product;

class PriceModifier
{
    /**
     * Specific price modification rules.
     *
     * @param Product $product
     *
     * @return int
     */
    public function modify(Product $product): int
    {
        $price = $product->getPrice();

        if ($price <= 100000) {
            return $price;
        }

        return str_starts_with($product->getSku(), 'WAT') ? // A watch
                $price * (1 + 0.15) :
                $price * (1 + 0.10)
            ;
    }
}
