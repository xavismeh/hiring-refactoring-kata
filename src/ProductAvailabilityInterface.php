<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto;

use MathildeGrise\Recrutement\KataRefacto\Models\Product;

interface ProductAvailabilityInterface
{
    /**
     * @return string[] Contains a key `Available`
     */
    public function getStockLevelByStore(int $storeId, Product $product): array;
}
