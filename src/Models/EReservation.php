<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Models;

class EReservation
{
    private int $id;
    private int $storeId;
    private string $productSku;
    private int $price;
    private int $customerId;

    public function __construct(int $id, int $storeId, string $productSku, int $price, int $customerId)
    {
        $this->id = $id;
        $this->storeId = $storeId;
        $this->productSku = $productSku;
        $this->price = $price;
        $this->customerId = $customerId;
    }
    public function getId(): int
    {
        return $this->id;
    }

    public function getStoreId(): int
    {
        return $this->storeId;
    }

    public function getProductSku(): string
    {
        return $this->productSku;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
    public function getCustomerId(): int
    {
        return $this->customerId;
    }
}
