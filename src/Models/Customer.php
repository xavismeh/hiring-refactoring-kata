<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Models;

class Customer
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
