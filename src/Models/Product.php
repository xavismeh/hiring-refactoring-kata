<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Models;

class Product
{
    private string $sku;
    private int $price;

    public function __construct(string $sku, int $price)
    {
        $this->sku = $sku;
        $this->price = $price;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}
