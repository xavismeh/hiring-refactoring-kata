<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\EventSubscriber;

use MathildeGrise\Recrutement\KataRefacto\HttpMailer;
use MathildeGrise\Recrutement\KataRefacto\Models\EReservation;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EReservationSubscriber implements EventSubscriberInterface
{
    public const ERESERVATION_NEW = 'ereservation.new';

    private LoggerInterface $logger;
    private HttpMailer $httpMailer;
    private string $salesTeamEmail;

    public function __construct(LoggerInterface $logger, HttpMailer $httpMailer, string $salesTeamEmail)
    {
        $this->logger = $logger;
        $this->httpMailer = $httpMailer;
        $this->salesTeamEmail = $salesTeamEmail;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            static::ERESERVATION_NEW => 'onNewEReservation',
        ];
    }

    public function onNewEReservation(EReservation $EReservation): void
    {
        try {
            $this->httpMailer->sendNewEReservation($this->salesTeamEmail, $EReservation->getId());
        } catch (\Throwable $e) {
            $this->logger->error('Error Send new Ereservation email', ['error' => $e->getMessage()]);
        }
    }
}
