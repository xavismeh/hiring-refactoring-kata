# Mathilde Grise - Refactoring Kata

## TODO before using the code
- Register the `MathildeGrise\Recrutement\KataRefacto\EventSubscriber\EReservationSubscriber` to the `EventDispatcher`'s instance.

## Helper
There is simple `Makefile` to help you to install dependencies and run the tests.
To prevent you to install build and runtime environment on your host, it uses Docker.
Feel free to use anything else.

Install dependencies: `make composer`

Run tests: `make tests`
