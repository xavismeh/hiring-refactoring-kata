<?php

declare(strict_types=1);

namespace MathildeGrise\Recrutement\KataRefacto\Tests;

use MathildeGrise\Recrutement\KataRefacto\CreateReservation;
use MathildeGrise\Recrutement\KataRefacto\Models\Customer;
use MathildeGrise\Recrutement\KataRefacto\Models\Product;
use MathildeGrise\Recrutement\KataRefacto\Models\Store;
use MathildeGrise\Recrutement\KataRefacto\PriceModifier;
use MathildeGrise\Recrutement\KataRefacto\ProductAvailabilityInterface;
use MathildeGrise\Recrutement\KataRefacto\Repositories\EReservationRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateReservationTest extends TestCase
{
    /**
     * @var CreateReservation
     */
    private $createReservation;

    /**
     * @var MockObject|LoggerInterface
     */
    private $logger;

    /**
     * @var ProductAvailabilityInterface|MockObject
     */
    private $productAvailability;

    /**
     * @var PriceModifier|MockObject
     */
    private $priceModifier;

    /**
     * @var EReservationRepository|MockObject
     */
    private $EReservationRepository;

    /**
     * @var EventDispatcherInterface|MockObject
     */
    private $eventDispatcher;

    protected function setUp(): void
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->productAvailability = $this->createMock(ProductAvailabilityInterface::class);
        $this->priceModifier = $this->createMock(PriceModifier::class);
        $this->EReservationRepository = $this->createMock(EReservationRepository::class);
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);

        $this->createReservation = new CreateReservation(
            $this->logger,
            $this->productAvailability,
            $this->priceModifier,
            $this->EReservationRepository,
            $this->eventDispatcher
        );
    }

    public function testCreateAvailabilityNotReachable(): void
    {
        $product = $this->createProductMock();

        $currentStore = $this->createStoreMock();
        $currentStore->expects($this->once())->method('getId')->willReturn(17);

        $this->productAvailability
            ->expects($this->once())
            ->method('getStockLevelByStore')
            ->with(
                $this->equalTo(17),
                $this->equalTo($product)
            )
            ->willThrowException(new \Exception())
        ;

        $output = $this->createReservation->create(
            $product,
            $this->createCustomerMock(),
            $currentStore
        );

        $this->assertSame(500, $output->getCode());
    }

    public function testCreateWithUnavailableProduct(): void
    {
        $product = $this->createProductMock();

        $currentStore = $this->createStoreMock();
        $currentStore->expects($this->once())->method('getId')->willReturn(17);

        $this->productAvailability
            ->expects($this->once())
            ->method('getStockLevelByStore')
            ->with(
                $this->equalTo(17),
                $this->equalTo($product)
            )
            ->willReturn(['Available' => false])
        ;

        $output = $this->createReservation->create(
            $product,
            $this->createCustomerMock(),
            $currentStore
        );

        $this->assertSame(412, $output->getCode());
    }

    public function testCreate(): void
    {
        $product = $this->createProductMock();
        $product->expects($this->once())->method('getSKU')->willReturn('test');

        $customer = $this->createCustomerMock();
        $customer->expects($this->once())->method('getId')->willReturn(1);

        $currentStore = $this->createStoreMock();
        $currentStore->expects($this->once())->method('getId')->willReturn(17);

        $this->productAvailability
            ->expects($this->once())
            ->method('getStockLevelByStore')
            ->with(
                $this->equalTo(17),
                $this->equalTo($product)
            )
            ->willReturn(['Available' => true])
        ;
        $this->EReservationRepository
            ->expects($this->once())
            ->method('nextId')
            ->willReturn(1)
        ;
        $this->priceModifier
            ->expects($this->once())
            ->method('modify')
            ->with($this->equalTo($product))
            ->willReturn(120)
        ;
        $this->EReservationRepository->expects($this->once())->method('save');

        $output = $this->createReservation->create($product, $customer, $currentStore);

        $this->assertSame(201, $output->getCode());
        $this->assertSame(1, $output->getData()['id']);
    }

    /**
     * @return MockObject|Product
     */
    private function createProductMock(): MockObject
    {
        return $this->createMock(Product::class);
    }

    /**
     * @return MockObject|Customer
     */
    private function createCustomerMock(): MockObject
    {
        return $this->createMock(Customer::class);
    }

    /**
     * @return MockObject|Store
     */
    private function createStoreMock(): MockObject
    {
        return $this->createMock(Store::class);
    }
}
